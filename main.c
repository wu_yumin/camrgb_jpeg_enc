
/*
 * Copyright (C) 2020 Yumin Wu <wuyuminst@163.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <getopt.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "camrgb_control.h"
#include "aiq_control.h"

#define CAMERA_WIDTH 1280
#define CAMERA_HEIGHT 720

int jpeg_demo_init(void)
{
    aiq_control_alloc();
	for (int i = 0; i < 10; i++) {
		if (aiq_control_get_status(AIQ_CONTROL_RGB)) {
			printf("%s: RGB aiq status ok.\n", __func__);
			camrgb_control_init();
			break;
		}
		sleep(1);
	}

    return 0;
}

void jpeg_demo_exit(void)
{
    camrgb_control_exit();
}

int main(int argc, char *argv[])
{
    int i;
    set_rgb_param(CAMERA_WIDTH, CAMERA_HEIGHT, true);
    jpeg_demo_init();
    sleep(2);
    for(i = 0; i < 5; i++) {
        camrgb_control_set_snapshot();
        sleep(2);
    }
    jpeg_demo_exit();
    printf("JpegDemo: finished!\n");
    return 0;
}

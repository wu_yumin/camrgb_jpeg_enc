# camrgb_jpeg_enc



## 概述

本程序项目使用了 Rockchip SDK 的以下模块： camera_engine_rkaiq，rga, mpp，实现一个可在RK平台上使用 MPP 进行 JPEG 编码的简单 Demo，可作为 camera sensor 开发调试的抓图工具使用。目前，程序在 RV1109 / RV1126 平台上调试运行OK。

程序的功能：

程序运行之后，每隔2秒对从 RGB 摄像头获取到的一帧数据进行 JPEG 编码，并且保存到文件中，一共保存5帧图片。

程序的主要流程如下：

- 使用 aiq 模块完成 RGB 摄像头的初始化
- 通过 rkisp_api 去获取摄像头的每一帧数据
- 间隔2秒使用 mpp 对获取到的数据进行JPEG编码
- 将编码为 JPEG 的数据写入文件



## 编译

可在 Rockchip SDK 的 buildroot 编译此程序。

1. 把程序源码放置到 SDK 的 external/ 目录。
2. 在 SDK 的 buildroot/package/rockchip 目录下创建 camrgb_jpeg_enc 目录。
3. 在 buildroot/package/rockchip/camrgb_jpeg_enc 添加文件 Config.in。

```SHELL
config BR2_PACKAGE_CAMRGB_JPEG_ENC
    bool "camrgb_jpeg_enc: test jpeg encode"
```

4. 在 buildroot/package/rockchip/camrgb_jpeg_enc 添加文件 camrgb_jpeg_enc.mk。

```makefile
CAMRGB_JPEG_ENC_SITE = $(TOPDIR)/../external/camrgb_jpeg_enc
CAMRGB_JPEG_ENC_SITE_METHOD = local

CAMRGB_JPEG_ENC_DEPENDENCIES = camera_engine_rkaiq linux-rga mpp jpeg
CAMRGB_JPEG_ENC_CONF_OPTS += "-DCAMERA_ENGINE_RKAIQ=y"

$(eval $(cmake-package))
```

5. 修改 buildroot/package/rockchip/Config.in，添加：

```shell
source "package/rockchip/camrgb_jpeg_enc/Config.in"
```

6. 在 buildroot 目录执行 `make menuconfig` 将此程序选上。

7. 在 buildroot 目录执行 `make camrgb_jpeg_enc` 编译程序，最终可执行文件 camrgb_jpeg_enc 被安装到 buildroot/output 的目标目录下。
8. 如果在 external/camrgb_jpeg_enc 中修改完代码需要重新编译，可在 buildroot 目录执行：

```shell
make camrgb_jpeg_enc-dirclean
make camrgb_jpeg_enc-rebuild
```



## 运行

camrgb_jpeg_enc 最终被安装到系统的 /usr/bin 目录，重新打包根文件系统固件并烧录可在设备运行程序。

也可以直接从 buildroot/output 中将 camrgb_jpeg_enc 复制出来，使用 adb 或者其他方法把 camrgb_jpeg_enc 放置到设备上。在运行程序之前可能需要给 camrgb_jpeg_enc 加上可执行权限：

```shell
chmod +x camrgb_jpeg_enc
```

使用 `camrgb_jpeg_enc` （程序在系统路径下），或者 `./camrgb_jpeg_enc` （程序不在系统路径下）即可运行程序。
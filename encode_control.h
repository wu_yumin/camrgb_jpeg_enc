
/*
 * Copyright (C) 2020 Yumin Wu <wuyuminst@163.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ENCODE_CONTROL_H__
#define __ENCODE_CONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "vpu_encode.h"
#include "rga_control.h"


#define NAME_LEN 256

struct enc_ctl {
    struct vpu_encode enc;
    bo_t enc_bo;
    int enc_fd;
    int size;
    char name[NAME_LEN];
    struct timeval t0;
    struct timeval t1;
};

int enc_ctl_init(struct enc_ctl *e, int w, int h);
void enc_ctl_exit(struct enc_ctl *e);
int enc_ctl_run(struct enc_ctl *e, int src_fd, void *src_buf, int width, int height, 
						RgaSURF_FORMAT fmt, int rotation);

#ifdef __cplusplus
}
#endif

#endif /* __ENCODE_CONTROL_H__ */
